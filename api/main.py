from typing import Union
from fastapi import FastAPI
import psycopg

app = FastAPI()


@app.get("/api/categories")
def get_N_hundred_categories(page: int = 0):        # sets the default to zero if no page# specified
    with psycopg.connect("dbname=trivia-game user=trivia-game") as conn:
        with conn.cursor() as cur:
            cur.execute("""
                SELECT id, title, canon
                FROM categories
                LIMIT 100 OFFSET %s;
            """, [page * 100])
            results = []
            all_of_it = cur.fetchall()
            for each in all_of_it:
                result = {
                    'id': each[0],
                    'title': each[1],
                    'canon': each[2],
                }
                results.append(result)

            return results

# the loop is to return a list of objects instead of a list of lists, because it reads better.
# cur.description was used in the lecture, unsure how to use it here tho


@app.post("/api/categories")
def create_new_false_canon_category(category: str):
    with psycopg.connect("dbname=trivia-game user=trivia-game") as conn:
        with conn.cursor() as cur:
            cur.execute("""
                INSERT INTO categories (title, canon)
                VALUES (%s, False)
            """,
                [category],
            )
        row = cur.fetchone()
        result = {}
        for i, column in enumerate(cur.description):
            result[column.name] = row[i]
        return result

# I don't know why ^ isn't working, follow up later.